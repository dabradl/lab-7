/**************************
 *Name: David Bradley
 *CPSC 1021-004
 *Your email: dabradl@clemson.edu
 *Section TA: Matt and Elliot
 **************************/
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;


typedef struct Employee {
	string lastName;
	string firstName;
	int birthYear;
	double hourlyWage;
}employee;

bool name_order(const employee& lhs, const employee& rhs);
int myrandom(int i) { return rand() % i; }


int main(int argc, char const *argv[]) {
	// IMPLEMENT as instructed below
	/*This is to seed the random generator */
	srand(unsigned(time(0)));


	/*Create an array of 10 employees and fill information from standard input with prompt messages*/
	employee employee_array[10]; //array delcaration
	for (int i = 0; i < 10; i++) {
		cout << "Enter the employee's last name.\n";
		cin >> employee_array[i].lastName;                // for loop to initialize the array with user input
		cout << "Enter the employee's first name.\n";    // includes output prompts asking for the input desired
		cin >> employee_array[i].firstName;
		cout << "Enter the employee's birth year.\n";
		cin >> employee_array[i].birthYear;
		cout << "Enter the employee's hourly wage.\n";
		cin >> employee_array[i].hourlyWage;
	}



	/*After the array is created and initialzed we call random_shuffle() see the
	 *notes to determine the parameters to pass in.*/

	random_shuffle(&employee_array[0], &employee_array[9], myrandom); //function used to randomize the array from the beginning and end points

	/*Build a smaller array of 5 employees from the first five cards of the array created
	 *above*/

	employee shuffled_employees[5] = { employee_array[0], employee_array[1], employee_array[2], employee_array[3], employee_array[4] };
	//creating a smaller array to hold the randomized employees


	/*Sort the new array.  Links to how to call this function is in the specs
	 *provided*/
	 //     starting point      ending point           comparator that sorts by name
	sort(shuffled_employees, shuffled_employees + 5, name_order);

	/*Now print the array below */

	//for loop to output the array of employees (uses output manipulation)
	for (int j = 0; j < 5; j++) {
		cout << right << setw(9) << shuffled_employees[j].lastName << ", " << shuffled_employees[j].firstName << endl;
		cout << right << setw(16) << shuffled_employees[j].birthYear << endl;
		cout << right << setw(16) << fixed << std::setprecision(2) << shuffled_employees[j].hourlyWage;

		if (j == 4) {
			break;   //makes it so that it will not print a new line on the last iteration of the loop ?
		}
		cout << endl;
	}



	return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
	// IMPLEMENT
	return lhs.lastName < rhs.lastName; //comparing the lastName part
}

